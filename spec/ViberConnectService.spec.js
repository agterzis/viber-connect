import { ExpressDataApplication } from '@themost/express';
import api from '../../../dist/server/app';
import { ViberConnectService } from '../src/ViberConnectService';
import path from 'path';
import { DataConfigurationStrategy } from '@themost/data';
import { TestUtils } from '../../../dist/server/utils';

describe('ViberConnectService', () => {

    /**
     * @type {import('@themost/express').ExpressDataApplication}
     */
    let app;

    beforeAll(() => {
        app = api.get(ExpressDataApplication.name);
        const adapters = app.getConfiguration().getSourceAt('adapters');
        const adapter = adapters.find(x => x.default === true);
        adapter.options.database = path.resolve('../../', adapter.options.database);
        app.getConfiguration().setSourceAt('adapters', adapters);

        let mailServer = app.getConfiguration().getSourceAt('settings/mail');
        if (mailServer == null) {
            mailServer = {};
        }
        mailServer.port = 1025;
        mailServer.host = 'localhost';
        mailServer.ignoreTLS = true;
        mailServer.from = 'sis-no-reply@auth.gr'
        app.getConfiguration().setSourceAt('settings/mail', mailServer);
    })

    it('should register service', async () => {
        app.useService(ViberConnectService);
        const model = app.getConfiguration().getStrategy(DataConfigurationStrategy).getModelDefinition('ViberConnectAction');
        expect(model).toBeTruthy();
        const eventListenerPath = path.resolve(__dirname, '../src/listeners/OnActionCompleteListener');
        const found = model.eventListeners.find(x => x.type === eventListenerPath);
        expect(found).toBeTruthy();
    });

    it('should add a new student request action', async () => {
        app.useService(ViberConnectService);
        const context = app.createContext();
        context.user = {
            name: 'student3@example.com'
        }
        await TestUtils.executeInTransaction(context, async () => {
            let newAction = {
                'name': 'A test viber connect aciton'
            }
            await context.model('ViberConnectAction').save(newAction);
            const item = await context.model('ViberConnectAction').where('id').equal(newAction.id).getItem();
            item.actionStatus = {
                alternateName: 'CompletedActionStatus'
            }
            await context.model('ViberConnectAction').silent().save(item);
            expect(item).toBeTruthy();
        })
        await context.finalizeAsync();
    });

});