import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class InstantNotificationReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('InstantNotification');

    if (model) {
      model.eventListeners = model.eventListeners || [];
      model.eventListeners.push({
        type: path.resolve(__dirname, 'listeners/OnSendingNewInstantNotification')
      });
      schemaLoader.setModelDefinition(model);
    }
  }

}

export {
  InstantNotificationReplacer
}
