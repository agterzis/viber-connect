/* eslint-disable no-unused-vars */
import { ApplicationService } from '@themost/common';
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { AccountReplacer } from './AccountReplacer';
import { viberConnectRouter } from './viberConnectRouter';
import { InstantNotificationReplacer } from './InstantNotificationReplacer';
import * as cheerio from 'cheerio';
import fetch from 'node-fetch';

export class ViberConnectService extends ApplicationService {
  constructor(app) {
    new AccountReplacer(app).apply();
    new InstantNotificationReplacer(app).apply();

    super(app);

    this.viberConfig = app.getConfiguration().getSourceAt('settings/universis/viber-connect');

    // extend universis api scope access configuration
    if (app && app.container) {
      app.container.subscribe((container) => {
        if (container) {
          // use router
          container.use('/services/viber', viberConnectRouter(app));

          // add extra scope access elements
          const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
          if (scopeAccess != null) {
            scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
          }
        }
      });
    }
  }

  /**
   * @param {import('@themost/express').ExpressDataContext} context 
   * @param {*} viberConnectAction 
   */
  async cancelAction(context, viberConnectAction) {
    if (context == null) {
      throw new Error('Context cannot be null');
    }

    if (viberConnectAction == null) {
      throw new Error('Action cannot be null');
    }

    const action = await context.model('ViberConnectAction').where('identifier').equal(viberConnectAction.identifier).silent().getItem();
    if (action == null) {
      throw new Error('Action not found');
    }

    if (action.actionStatus.alternateName !== 'CancelledActionStatus') {
      action.actionStatus = {
        alternateName: 'CancelledActionStatus',
      }
      await context.model('ViberConnectAction').silent().save(action);
    }
  }

  /**
   * @param {import('@themost/express').ExpressDataContext} context 
   */
  async getApifonToken(context) {
    const token = await fetch(this.viberConfig.apifon.tokenURL, {
      method: 'POST',
      body: new URLSearchParams({
        client_id: this.viberConfig.apifon.client_id,
        client_secret: this.viberConfig.apifon.client_secret,
        grant_type: this.viberConfig.apifon.grant_type
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(res => res.json());

    if (!token || !token.access_token) {
      throw new Error('Could not get apifon token');
    }

    return token;
  }

   /**
   * @param {import('@themost/express').ExpressDataContext} context 
   */
   async sendMessage(context, messagingChannel, member, body) {
    try {
	    const token = await this.getApifonToken(context);
      const accessToken = token.access_token;
      const viberAccount = member.account.viberAccount;
      const phone = viberAccount.identifier.replace('+', '');
      const $ = cheerio.load(body);
      let bodyText = $('body').find('br').replaceWith('\n').end().text();

      // const messageSubject = `[${messagingChannel.alternateName}] New Notification!`
      const messageBody = `You have a new notification on channel ${messagingChannel.alternateName}:\n\n${bodyText}\n\nYour phone number: ${phone}`;

      const msg = await fetch(`${this.viberConfig.apifon.hostURL}/services/api/v1/im/send`, {
        method: 'POST',
        body: JSON.stringify({
          message: {
            text: messageBody,
            sender_id: 'AUTH-IT'
          },
          subscribers: [
            {
              number: phone
            }
          ],
          im_channels: [
            {
              text: messageBody
            }
          ]
        }),
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json());
    } catch (err) {
      console.log('Could not send message via APIFON: ', err);
    }
  }

  /**
   * @param {import('@themost/express').ExpressDataContext} context 
   */
  async sendOtp(context, person, viberConnectAction) {
    try {
      const token = await this.getApifonToken(context);
      const accessToken = token.access_token;
      const phone = (person.mobilePhone ?? person.temporaryPhone).replace('+', '');

      const messageBody = `Here is your OTP code:\n${viberConnectAction.pin}\nExpires in 5 minutes.\n`

      const msg = await fetch(`${this.viberConfig.apifon.hostURL}/services/api/v1/im/send`, {
        method: 'POST',
        body: JSON.stringify({
          message: {
            text: messageBody,
            sender_id: 'AUTH-IT'
          },
          subscribers: [
            {
              number: phone
            }
          ],
          im_channels: [
            {
              text: messageBody
            }
          ]
        }),
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json());
    } catch (err) {
      console.log('Could not send OTP message via APIFON: ', err)
    }
  }

}