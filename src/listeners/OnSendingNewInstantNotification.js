/* eslint-disable no-unused-vars */
import { DataError } from '@themost/common';
import { DataEventArgs, DataObjectState } from '@themost/data';
import { ExpressDataContext } from '@themost/express';
import _ from 'lodash';
import { ViberConnectService } from '../ViberConnectService';

// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
	//
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	// get context
	/**
	 * @type {ExpressDataContext}
	 */
	const context = event.model.context;

	/**
	 * @type {ViberConnectService}
	 */
	const service = context.getApplication().getService(ViberConnectService);

	const messagingChannel = await context.model('MessagingChannel').find(event.target.recipient).silent().getTypedItem();
	if (messagingChannel == null) {
		return;
	}

	const members = await messagingChannel.property('members')
		.where('account/viberAccount/identifier').notEqual(null)
		.and('id').notEqual(event.target.owner)
		.expand({
			name: 'account',
			options: {
				$expand: 'viberAccount'
			}
		})
		.silent()
		.getAllItems();

	// get all members except message sender && those who do not have a viber account
	// members = members.filter((x) => x.id !== event.target.owner && x.account.viberAccount != null);

	for (const member of members) {
		const body = event.target.body;
		service.sendMessage(context, messagingChannel, member, body);
	}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	return beforeSaveAsync(event).then(() => {
		return callback();
	}).catch((err) => {
		return callback(err);
	});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	return afterSaveAsync(event).then(() => {
		return callback();
	}).catch((err) => {
		return callback(err);
	});
}