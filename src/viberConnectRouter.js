/* eslint-disable quotes */
/* eslint-disable no-unused-vars */
import { ExpressDataApplication } from "@themost/express";
import { ViberConnectService } from "./ViberConnectService";
import path from 'path';
import { Router } from 'express';
import { DataError, HttpBadRequestError, HttpError, HttpUnauthorizedError } from '@themost/common';
import { Passport } from 'passport';
import OAuth2Strategy from 'passport-oauth2';
import rateLimit from 'express-rate-limit';
import cookieSession from 'cookie-session';

class InvalidOrExpiredTokenError extends HttpUnauthorizedError {
  constructor(msg) {
    super(msg || 'invalid/expired token');
    this.status = 401;
    this.code = '1040';
  }
}

/**
 * @param {ExpressDataApplication} app
 * @returns {*}
 */
function viberConnectRouter(app) {
  /**
   * @type {ViberConnectService}
   */
  const service = app.getService(ViberConnectService);
  const viberConfig = service.getApplication().getConfiguration().getSourceAt('settings/universis/viber-connect');

  const router = Router();
  const passport = new Passport();

  /**
    * @type {{ authorizationURL: string, tokenURL: string, callbackURL: string }}
    */
  if (viberConfig.auth == null) {
    throw new Error('ViberConnectService configuration may not be null');
  }

  /**
   * START: Handle session and context
   */
  passport.use(new OAuth2Strategy(Object.assign(viberConfig.auth, {
    passReqToCallback: true,
  }), function (req, accessToken, refreshToken, profile, done) {
    /**
      * Gets OAuth2 client services
      * @type {*}
      */
    const client = req.context.getApplication().getStrategy(function OAuth2ClientService() { });
    const translateService = req.context.getApplication().getService(function TranslateService() { });
    // if client cannot be found
    if (typeof client === 'undefined') {
      // throw configuration error
      return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
    }
    if (accessToken == null) {
      // throw 499 Token Required error
      return done(new HttpError(499, 'A token is required to fulfill the request.', 'Token Required'));
    }
    // get token info
    client.getTokenInfo(req.context, accessToken).then(info => {
      if (info == null) {
        // the specified token cannot be found - 498 invalid token with specific code
        return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
      }
      // if the given token is not active throw token expired - 498 invalid token with specific code
      if (!info.active) {
        return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
      }

      return done(null, {
        "name": info.username,
        "authenticationType": 'AccessToken',
        "authenticationToken": accessToken,
        "authenticationScope": info.scope
      });
    }).catch(err => {
      const statusCode = (err && err.statusCode) || 500;
      if (statusCode === 404) {
        // revert 404 not found returned by auth server to 498 invalid token
        return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
      }
      // otherwise continue with error
      return done(err);
    });
  }));

  passport.serializeUser(function (user, done) {
    return done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    return done(null, user);
  });

  // configure rate limit
  const rateLimitOptions = Object.assign({
    windowMs: 5 * 60 * 1000, // 5 minutes
    max: 30, // 30 requests
    standardHeaders: true,
    legacyHeaders: false,
    keyGenerator: (req) => {
      return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection ? req.connection.remoteAddress : req.socket.remoteAddress);
    }
  }, app.getConfiguration().getSourceAt('settings/universis/rateLimit'));
  /**
   * @type {RequestHandler}
   */
  const rateLimitHandler = rateLimit(rateLimitOptions);

  const secret = app.getConfiguration().getSourceAt('settings/crypto/key');

  // use client-side session
  // https://github.com/expressjs/cookie-session#cookie-session
  const sessionCookieOptions = {
    keys: [
      secret
    ],
    name: 'viber-connect.sid'
  };
  router.use(cookieSession(sessionCookieOptions));

  // initialize passport
  router.use(passport.initialize());
  router.use(passport.session());

  // create context middleware
  router.use((req, _res, next) => {
    // create router context
    const newContext = app.createContext();
    /**
     * try to find if request has already a data context
     * @type {ExpressDataContext|*}
     */
    const interactiveContext = req.context;
    // finalize already assigned context
    if (interactiveContext) {
      if (typeof interactiveContext.finalize === 'function') {
        // finalize interactive context
        return interactiveContext.finalize(() => {
          // and assign new context
          Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
              return newContext
            }
          });
          // exit handler
          return next();
        });
      }
    }
    // otherwise assign context
    Object.defineProperty(req, 'context', {
      enumerable: false,
      configurable: true,
      get: () => {
        return newContext
      }
    });
    // and exit handler
    return next();
  });

  // set locale middleware
  router.use((req, _res, next) => {
    // set context locale from request
    req.context.locale = req.locale;
    // set translate
    const translateService = req.context.application.getStrategy(function TranslateService() { });
    req.context.translate = function () {
      if (translateService == null) {
        return arguments[0];
      }
      return translateService.translate.apply(translateService, Array.from(arguments));
    };
    return next();
  });

  router.use((req, _res, next) => {
    req.context.user = req.session.user;
    return next();
  });

  // use this handler to finalize router context
  // important note: context finalization is very important in order
  // to close and finalize database connections, cache connections etc.
  router.use((req, _res, next) => {
    req.on('end', () => {
      //on end
      if (req.context) {
        //finalize data context
        return req.context.finalize(() => {
          //
        });
      }
    });
    return next();
  });

  // handle rate limit
  router.use(rateLimitHandler);
  /**
   * END: Handle session and context
   */

  router.get('/', (req, res) => {
    return res.redirect('/services/viber/index');
  })

  router.post('/deleteAccount', (req, res, next) => {
    // if user is not logged in redirect to login page
    if (!req.context.user) {
      return res.redirect('/services/viber/login');
    }

    // otherwise continue
    return next();
  }, async (req, res, next) => {
    const user = await req.context.model('Users')
      .where('name').equal(req.context.user.name)
      .expand('viberAccount')
      .silent()
      .getItem();

    if (user.viberAccount == null) {
      return res.status(200).redirect('/services/viber/index');
    }

    user.viberAccount = null;
    await req.context.model('Users').silent().save(user);

    return res.status(200).redirect('/services/viber/index');
  });

  // index page
  router.get('/index', (req, res, next) => {
    // if user is not logged in redirect to login page
    if (!req.context.user) {
      return res.redirect('/services/viber/login');
    }

    // otherwise continue
    return next();
  }, async (req, res, next) => {
    try {
      // If the user has already got a ViberAccount, then render the profile page
      const user = await req.context.model('Users')
        .where('name').equal(req.context.user.name)
        .expand('viberAccount')
        .silent()
        .getItem();

      if (user.viberAccount != null) {
        return res.status(200).render(path.resolve(__dirname, './views/profile'), {
          logoutURL: viberConfig.auth.logoutURL,
          user: user
        })
      }

      // Otherwise render the normal index page
      const viberConnectAction = await req.context.model('ViberConnectAction')
        .asQueryable()
        .expand('owner')
        .where('owner/name').equal(req.context.user.name)
        .and('actionStatus/alternateName').equal('ActiveActionStatus')
        .silent()
        .getItem();

      // Check if endTime passed
      if (viberConnectAction != null) {
        if (viberConnectAction.endTime < new Date()) {
          await service.cancelAction(req.context, viberConnectAction);
        }
      }

      const person = await req.context.model('Student')
        .asQueryable()
        .expand({
          name: 'user',
        }, {
          name: 'person',
        })
        .where('user/name').equal(req.context.user.name)
        .select('person')
        .silent()
        .value();

      res.status(200).render(path.resolve(__dirname, './views/index'), {
        person: person,
        viberConnectAction: viberConnectAction,
        logoutURL: viberConfig.auth.logoutURL
      });
    } catch (err) {
      return next(err);
    }
  });

  router.post('/index', (req, res, next) => {
    // if user is not logged in redirect to login page
    if (!req.context.user) {
      return res.redirect('/services/viber/login');
    }
    // otherwise continue
    return next();
  }, async (req, res, next) => {
    // 1. create ViberConnectAction with a status of 'ActiveActionStatus' if another one does not exist
    const viberConnectAction = await req.context.model('ViberConnectAction')
      .asQueryable()
      .expand('owner')
      .where('owner/name').equal(req.context.user.name)
      .and('actionStatus/alternateName').equal('ActiveActionStatus')
      // .and('endTime').greaterThan(new Date())
      .silent()
      .getItem();

    if (viberConnectAction != null) {
      if (viberConnectAction.endTime < new Date()) {
        await service.cancelAction(req.context, viberConnectAction);
      }
      // A ViberConnectAction already exists, redirect to otp page
      return res.status(200).redirect('/services/viber/otp');
    }

    // create ViberConnectAction
    let newViberConnectAction = await req.context.model('ViberConnectAction').silent().save({
      actionStatus: {
        alternateName: 'ActiveActionStatus'
      }
    });

    // 3 (?). send PIN (OTP code) to user ----> this happens in the listener (OnActionCompleteListener)
    // 4. redirect to OTP page (GET)
    return res.status(200).redirect('/services/viber/otp');
  });

  // resend otp page
  router.post('/sendNewOtp', (req, res, next) => {
    // if user is not logged in redirect to login page
    if (!req.context.user) {
      return res.redirect('/services/viber/login');
    }
    // otherwise continue
    return next();
  }, async (req, res, next) => {
    // 1. if a ViberConnectAction already exists && is active && has not expired then show error message "you have to wait 5 minutes"
    const viberConnectAction = await req.context.model('ViberConnectAction')
      .asQueryable()
      .expand('owner')
      .where('owner/name').equal(req.context.user.name)
      .and('actionStatus/alternateName').equal('ActiveActionStatus')
      .silent()
      .getItem();

    if (viberConnectAction == null || viberConnectAction.endTime < new Date()) {
      if (viberConnectAction != null) {
        await service.cancelAction(req.context, viberConnectAction);
      }

      // create new ViberConnectAction (pin will be automatically sent by the listener OnActionCompleteListener)
      let newViberConnectAction = await req.context.model('ViberConnectAction').silent().save({
        actionStatus: {
          alternateName: 'ActiveActionStatus'
        }
      });

      // redirect to otp page
      return res.status(200).redirect('/services/viber/otp');
    }

    // this checks whether the ViberConnectAction has expired
    if (viberConnectAction.endTime > new Date()) {
      // show error message "you have to wait 5 minutes"
      return res.status(200).render(path.resolve(__dirname, './views/otp'), {
        viberConnectAction: viberConnectAction,
        error: "You have to wait 5 minutes",
      });
    }

    return res.status(200).redirect('/services/viber/otp');
  });

  router.get('/otp', (req, res, next) => {
    if (!req.context.user) {
      return res.redirect('login');
    }
    return next();
  },
    async (req, res, next) => {
      try {
        // 1. Check if a ViberConnectAction by this user exists and is ActiveActionStatus
        const viberConnectAction = await req.context.model('ViberConnectAction')
          .asQueryable()
          .expand('owner')
          .where('owner/name').equal(req.context.user.name)
          .and('actionStatus/alternateName').equal('ActiveActionStatus')
          // .and('endTime').greaterThan(new Date())
          .silent()
          .getItem();

        // (2a). If a ViberConnectAction does not exist, or is not active, then redirect to /index
        if (viberConnectAction == null) {
          return res.redirect('/services/viber/index');
        }

        if (viberConnectAction != null) {
          if (viberConnectAction.endTime < new Date()) {
            await service.cancelAction(req.context, viberConnectAction);
          }
        }

        // 2. If a ViberConnectAction exists and is active, then show the OTP page view
        res.status(200).render(path.resolve(__dirname, './views/otp'), {
          viberConnectAction: viberConnectAction,
          error: null,
        });
      } catch (err) {
        return next(err);
      }
    });


  router.post('/otp', (req, res, next) => {
    // if user is not logged in redirect to login page
    if (!req.context.user) {
      return res.redirect('/services/viber/login');
    }
    // otherwise continue
    return next();
  }, async (req, res, next) => {
    try {
      const { pin } = req.body; // Λαμβάνετε το PIN από το αίτημα (request)

      /**
       * FLOW:
       */
      // 1. Check if a ViberConnectAction by this user exists and is ActiveActionStatus
      // 2. Validate the following:
      //    a. Check if the PIN == the PIN in the ViberConnectAction
      //    b. Check if the endTime < now (this means that the PIN has expired)
      //    (?) c. Check if the ViberConnectAction owner == the current logged in user

      // 4. If the validations are successful, then change the ViberConnectAction status to 'CompletedActionStatus'

      const viberConnectAction = await req.context.model('ViberConnectAction')
        .asQueryable()
        .expand('owner')
        .where('owner/name').equal(req.context.user.name)
        .and('actionStatus/alternateName').equal('ActiveActionStatus')
        .silent()
        .getItem();

      if (viberConnectAction == null) {
        return res.redirect('/services/viber/index');
      }

      if (viberConnectAction != null) {
        if (viberConnectAction.endTime < new Date()) {
          await service.cancelAction(req.context, viberConnectAction);
          return res.status(200).redirect('/services/viber/index');
        }
      }

      if (viberConnectAction.pin !== pin) {
        // pin is wrong
        return res.status(200).render(path.resolve(__dirname, './views/otp'), {
          viberConnectAction: viberConnectAction,
          error: "Invalid PIN code",
        });
      }

      viberConnectAction.actionStatus = { 'alternateName': 'CompletedActionStatus' };
      await req.context.model('ViberConnectActions').silent().save(viberConnectAction);

      // res.status(200).redirect('/services/viber/successVerification');

      const person = await req.context.model('Student')
        .asQueryable()
        .expand({
          name: 'user',
        }, {
          name: 'person',
        })
        .where('user/name')
        .equal(req.context.user.name)
        .select('person')
        .silent()
        .value();

      res.status(200).render(path.resolve(__dirname, './views/successVerification'), {
        viberConnectAction: viberConnectAction,
        person: person,
        logoutURL: viberConfig.auth.logoutURL,
      });
    } catch (err) {
      return next(err);
    }
  });

  // login
  router.get('/login', passport.authenticate('oauth2', {
    session: true,
    scope: [
      'profile'
    ]
  }));

  // logout
  router.get('/logout', (req, res, next) => {
    req.session = null;
    return res.status(200).render(path.resolve(__dirname, './views/logout'));
  });

  // /callback route
  router.get('/callback', passport.authenticate('oauth2', {
    failureRedirect: 'login',
    session: true,
    scope: [
      'profile'
    ]
  }), (req, res, next) => {
    req.context.user = req.session.user = req.user;
    return res.redirect('/services/viber/index');
  });

  return router;
}

export {
  viberConnectRouter
}
